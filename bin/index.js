#!/usr/bin/env node


var colors = require("colors"),
    fs = require("fs"),
    filePath = require("@nathanfaucett/file_path");


var task = module.exports = require(findLocalModule("@nathanfaucett/task", "..")),
    args = process.argv.slice(2),
    command = args[0] || "default",
    help = args[1] || args[0],
    tmp;


if (command === "--help" || command === "-h") {
    tmp = help;
    help = command;
    command = tmp;
}


function findLocal(id, type, name) {
    var root = process.cwd(),
        depth = root.split(filePath.separator).length,
        fullPath = filePath.join(root, id);

    if (fs.existsSync(fullPath)) {
        return fullPath;
    } else {
        while (depth--) {
            fullPath = filePath.join(root, id);
            root = filePath.join(root, "..");

            if (fs.existsSync(fullPath)) {
                return fullPath;
            }
        }
        throw new Error("could not find " + (type || "file") + " " + (name || id));
    }
}

function findLocalModule(name, fallback) {
    try {
        return filePath.dirname(findLocal(filePath.join("node_modules", name, "package.json"), "node module", name));
    } catch(e) {
        console.log(
            "[" + colors.grey(formatDate(new Date())) + "]",
            colors.red("Failed to find local @nathanfaucett/task package using global package instead")
        );
        return fallback;
    }
}


require(findLocal("taskfile.js"));


function forceSize(value, size) {
    var string = '' + value;

    size = size || 2;

    while (string.length < size) {
        string = '0' + string;
    }

    return string;
}

function formatDate(date) {
    return (
        forceSize(date.getHours()) + ':' +
        forceSize(date.getMinutes()) + ':' +
        forceSize(date.getSeconds()) + '.' +
        forceSize(date.getMilliseconds(), 3)
    );
}

if (help === "--help" || help === "-h") {
    console.log("\n\rtask help:\n\r\n\r" + task.help(command));
} else {

    task.on("Task.start", function onStart(task) {
        console.log(
            "[" + colors.grey(formatDate(new Date(task.startTime))) + "]",
            "Starting \"" + colors.cyan(task.name) + "\" ..."
        );
    });
    task.on("Task.end", function onStart(task) {
        console.log(
            "[" + colors.grey(formatDate(new Date(task.endTime))) + "]",
            "Finished \"" + colors.cyan(task.name) + "\"",
            "after " + colors.magenta(((task.endTime - task.startTime) | 0) + " ms")
        );
    });

    task.run(command, function onDone(error) {
        if (error) {
            throw error;
        }
    });
}
